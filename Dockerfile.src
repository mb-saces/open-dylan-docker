ARG MB_DISTRO
ARG MB_VERSION
ARG MB_ARCHIVE_REGISTRY
ARG MB_BIN_REGISTRY

FROM ${MB_ARCHIVE_REGISTRY}/archives:testing as archives

FROM ${MB_BIN_REGISTRY}/opendylan-bin:${MB_DISTRO}-${MB_VERSION} as base

ARG MB_DISTRO

RUN if [ "$MB_DISTRO" = "ubuntu" ]; then \
      apt-get update \
      && apt-get upgrade -y \
      && DEBIAN_FRONTEND="noninteractive" apt-get install --no-install-recommends -y clang-10 llvm-10 \
      && apt-get autoremove --purge \
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/*; \
    fi

RUN if [ "$MB_DISTRO" = "debian" ]; then \
      apt-get update \
      && apt-get upgrade -y \
      && DEBIAN_FRONTEND="noninteractive" apt-get install --no-install-recommends -y clang llvm \
      && apt-get autoremove --purge \
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/*; \
    fi

RUN if [ "$MB_DISTRO" = "fedora" ]; then \
      dnf -y install clang llvm; \
    fi

RUN if [ "$MB_DISTRO" = "alpine" ]; then \
      apk add --no-cache clang10 llvm10; \
    fi

FROM base as builder

ARG MB_DISTRO

RUN if [ "$MB_DISTRO" = "ubuntu" -o "$MB_DISTRO" = "debian" ]; then \
      apt-get update \
      && apt-get upgrade -y \
      && DEBIAN_FRONTEND="noninteractive" apt-get install --no-install-recommends -y autoconf automake git libgc-dev libunwind-dev \
      && apt-get autoremove --purge \
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/*; \
    fi

RUN if [ "$MB_DISTRO" = "fedora" ]; then \
      dnf -y install autoconf automake findutils perl-Time-HiRes libgc-devel libunwind-devel; \
    fi

RUN if [ "$MB_DISTRO" = "alpine" ]; then \
      apk add --no-cache autoconf automake libgc-dev libunwind-dev; \
    fi

WORKDIR /bob/archives

COPY --from=archives /archives/dylan/opendylan-master.tar.gz .
COPY --from=archives /archives/dylan/dylan-tool-master.tar.gz .

RUN mkdir -p /bob/opendylan \
    && tar -xzf opendylan-master.tar.gz -C /bob/opendylan --strip-components=1

RUN mkdir -p /bob/dylan-tool \
    && tar -xzf dylan-tool-master.tar.gz -C /bob/dylan-tool --strip-components=1

WORKDIR /bob/opendylan
RUN ./autogen.sh
RUN ./configure --prefix=/opt/opendylan-latest
RUN make
RUN make install

ENV PATH=/opt/opendylan-latest/bin:$ORIG_PATH
ENV DYLAN=/opt/opendylan-latest

WORKDIR /bob/dylan-tool
RUN make
RUN make install

FROM base

ENV PATH=/opt/opendylan-latest/bin:$ORIG_PATH
ENV DYLAN=/opt/opendylan-latest

COPY --from=builder /opt/opendylan-latest/* /opt/opendylan-latest/
